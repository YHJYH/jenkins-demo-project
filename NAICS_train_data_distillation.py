#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 11:10:36 2022

@author: n1603147

1. remove samples with incorrect naics code;

2. remove Named Entities in texts;

"""

import pandas as pd
import numpy as np
from typing import List, Tuple, Set
#from collections import Counter
#from pprint import pprint
import locationtagger
import multiprocessing as mp

import nltk
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.chunk import conlltags2tree, tree2conlltags
from nltk.corpus import stopwords
# essential entity models downloads
nltk.downloader.download('maxent_ne_chunker')
nltk.downloader.download('words')
nltk.downloader.download('treebank')
nltk.downloader.download('maxent_treebank_pos_tagger')
nltk.downloader.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('stopwords')

import spacy
from spacy import displacy

import en_core_web_sm
nlp = en_core_web_sm.load()

# Customer functions
from NAICS_utils import *

def training_data_reader(path: str) -> pd.DataFrame:
    """This function reads the training data from excel file to pd.DataFrame.
    
    Args:
        path: training data file location.
        
    Returns:
        df: training data dataframe.
    """
    if path.endswith('xlsx'):
        df = pd.read_excel(io=path, sheet_name='in')
    elif path.endswith('csv'): 
        df = pd.read_csv(path)
    return df

def invalid_naics_removal(df: pd.DataFrame) -> pd.DataFrame:
    """ This function removes instances that have incorrect naics codes.
    
    Args:
        df: training data dataframe before removal.
        
    Returns:
        df: training data dataframe after removal.
    """
    code = list(df)[1]
    naics_codes = NAICS_reader(naics_path)
    drop_rows = []
    for i in range(df.shape[0]):
        #if df[code][i] not in naics_codes: O(N^2)
        #    drop_rows.append(i)
        if not binary_search(naics_codes, df[code][i]): #O(log N)
            drop_rows.append(i)
    
    df = df.drop(labels=drop_rows, axis=0)
    df = df.reset_index(drop=True)
    return df

def duplicates_removal(df: pd.DataFrame) -> pd.DataFrame:
    """
    
    """
    drop_rows = []
    for i in range(1, df.shape[0]):
        if df['1_search'][i] == df['1_search'][i-1]:
            drop_rows.append(i)
    
    df = df.drop(labels=drop_rows, axis=0)
    df = df.reset_index(drop=True)
    return df

def df_to_local(df: pd.DataFrame, fileName: str, fileType: str) -> None:
    """This function convert the training data dataframe to a excel file.
    
    Args:
        df: training data dataframe.
        fileName: name of the file.
        fileType: type of the file. Options: excel or csv.
        
    Returns:
        None.
    """
    # .xlsx file is about 40% smaller than .csv file in size.
    if fileType == 'excel':
        with pd.ExcelWriter(fileName+'.xlsx') as writer:
            df.to_excel(writer, sheet_name='in', index=False)
    elif fileType == 'csv':
        df.to_csv(fileName+'.csv', index=False)


def spacy_NE_BILUO_tagging(sentence: str) -> List[Tuple[spacy.tokens.token.Token, str, str]]:
    """
    
    """
    doc = nlp(sentence)
    BILUO_tagged = [(x, x.ent_iob_, x.ent_type_) for x in doc]
    return BILUO_tagged

def remove_nonASCII(sentence: str) -> str:
    """This functions remove non-ASCII characters in a text input.
    
    """
    
    try:
        sent_encode = sentence.encode('ascii', 'ignore')
    except AttributeError:
        print(sentence)
    else:
        sent_decode = sent_encode.decode()
        return sent_decode
    

def remove_specialChar(sentence: str) -> str:
    """
    
    """
    sent = ''.join(c for c in sentence if c.isalnum() or c==' ')
    sent = ' '.join(sent.split())
    return sent

def remove_longNum(sentence: str) -> str:
    """
    
    """
    sent = [char for char in sentence.split() if not char.isnumeric()]
    sent = ' '.join(sent)
    
    return sent

def remove_stopwords(sentence: str) -> str:
    """
    
    """
    word_tokens = word_tokenize(sentence)
    filtered_sent = [w for w in word_tokens if w.lower() not in stop_words]
    return ' '.join(filtered_sent)

def remove_SPACY_NER(BILUO_tagged: List[Tuple[spacy.tokens.token.Token, str, str]], sentence: str) -> str:
    """
    
    """
    inSentTypes = [t[-1] for t in BILUO_tagged]
    commonTypes = ['ORG', 'NORP', 'MONEY', 'DATE', 'CARDINAL']
    # only commonly used types are considered
    sent = sentence.split()
    new_sent = [sent[i] for i in range(len(sent)) if inSentTypes[i] not in commonTypes]
    new_sent = ' '.join(new_sent)
    return new_sent

def loc_tagging(sentence: str, o: set, r: set) -> str:
    """
    
    """
    #place_entity = locationtagger.find_locations(text=sentence)
    #print(sentence)
    sent = sentence.split()
    #other = ' '.join(place_entity.other + place_entity.other_regions + place_entity.other_countries)
    sent = [char for char in sent if char not in o]
    #regions = list(place_entity.country_regions.values()) + list(place_entity.region_cities.values()) + list(place_entity.country_cities.values())
    #regions = ' '.join([' '.join(char) for char in regions])
    sent = [char for char in sent if char not in r]
    return ' '.join(sent)

def sent_loc_tagging(sentence_list: List[str]) -> np.ndarray:
    """
    
    """
    loc_arr = [''] * len(sentence_list)
    for i, sentence in enumerate(sentence_list):
        place_entity = locationtagger.find_locations(text=sentence)
        other = ' '.join(place_entity.other + place_entity.other_regions + place_entity.other_countries)
        regions = list(place_entity.country_regions.values()) + list(place_entity.region_cities.values()) + list(place_entity.country_cities.values())
        regions = [' '.join(region) for region in regions]
        regions = ' '.join(regions)
        locations = ' '.join(set(other.lower().split() + regions.lower().split()))
        loc_arr[i] = locations
    loc_arr = np.asarray(loc_arr)
    return loc_arr
        
    """
    global o
    global r
    passage = ' '.join(sentence_list)
    place_entity = locationtagger.find_locations(text=passage)
    other = ' '.join(place_entity.other + place_entity.other_regions + place_entity.other_countries)
    regions = list(place_entity.country_regions.values()) + list(place_entity.region_cities.values()) + list(place_entity.country_cities.values())
    regions = [' '.join(region) for region in regions]
    regions = ' '.join(regions)
    o = set(other.lower().split())
    r = set(regions.lower().split())
    """    
        
    #return other, regions

#def sent_loc_tagging(df: pd.DataFrame) -> List[str]:
    """
    passage_loc_tagging(df['1_search'])
    with mp.Pool() as pool:
        search1 = pool.map(loc_tagging, list(df['1_search']))
    
    return search1
    """

def apply_loc_arr(loc_arr: np.ndarray, df: pd.DataFrame) -> pd.DataFrame:
    """This function changes the training data dataframe in-place, removing all location info.
    
    """
    for i in range(len(loc_arr)):
        locations = loc_arr[i].split()
        search_result = df.at[i, '1_search'].split()
        modified_result = [w for w in search_result if not w in locations]
        df.at[i, '1_search'] = ' '.join(modified_result)
    return df

    
def file_inout(loc_arr: np.ndarray, mode: str, fileName: str) -> List[str]:
    """
    
    """
    if mode == 'write':
        with open(fileName, 'w') as fp:
            for item in loc_arr:
                fp.write("%s\n" % item.encode('utf-8'))
        print('Done!')
        return None
    elif mode == 'read':
        loc_arr = []
        with open(fileName, 'r') as fp:
            for line in fp:
                x = line[:-1]
                x = x[2:]
                loc_arr.append(x)
        print('Done!')
        return loc_arr
    

def dataframe_preprocess(training_data_path: str) -> pd.DataFrame:
    """
    
    """
    df = training_data_reader(training_data_path)
    df = invalid_naics_removal(df)
    df = df.dropna(how='all', subset=search_list)
    df = df.reset_index(drop=True)
    df = duplicates_removal(df)
    if df[search_list].isnull().values.any():
        df[search_list] = df[search_list].fillna('in ')
    df['1_search'] = df[search_list].T.agg(' '.join)
    return df
  
def sent_preprocess(df: pd.DataFrame) -> pd.DataFrame:  
    """
    
    """
    '''
    sent = remove_nonASCII(sentence)
    sent = remove_specialChar(sent)
    sent = remove_longNum(sent)
    sent = loc_tagging(sent)
    sent = remove_SPACY_NER(spacy_NE_BILUO_tagging(sent), sent)
    sent = remove_stopwords(sent)
    '''
    #remove non ASCII characters
    df['1_search'] = df['1_search'].apply(lambda x: x.encode('ascii', 'ignore').decode())
    #remove special characters
    df['1_search'] = df['1_search'].str.replace('[^\w\s]', '')
    #remove long numbers
    df['1_search'] = df['1_search'].str.replace('\d+', '')
    #make all characters in lower case
    df['1_search'] = df['1_search'].str.lower()
    #remove stopwords and location info
    df['1_search'] = df['1_search'].apply(remove_stopwords)
    
    #obtain locations info: ~3hrs
    loc_arr = sent_loc_tagging(list(df['1_search']))
    #write loc_arr into a .txt file
    file_inout(loc_arr, mode='write', fileName='loc_arr.txt')
    #remove locations info
    df = apply_loc_arr(loc_arr, df)
    
    return df
               
def full_distillation(training_data_path: str) -> pd.DataFrame:
    """
    
    """
    df = dataframe_preprocess(training_data_path)
    #search_list = ['%d_search' % i for i in range(1,11)]
    #for search in search_list:
    """
    for row in range(len(df)):
        sentence = df.at[row, '1_search']
        #if sentence is None: continue
        sent = remove_nonASCII(sentence)
        sent = remove_specialChar(sent)
        sent = remove_longNum(sent)
        sent = loc_tagging(sent)
        #if sent is None: continue
        sent = remove_SPACY_NER(spacy_NE_BILUO_tagging(sent), sent)
        sent = remove_stopwords(sent)
        df.at[row, '1_search'] = sent
    """
    
    #df_to_local(df, 'distilled_dataset', 'csv')
    return df
    
search_list = ['%d_search' % i for i in range(1,11)]
stop_words = set(stopwords.words('english'))

train_data_path = 'training_data_new.xlsx'
naics_path = '2022_NAICS_Structure.xlsx'
new_train_data_path = 'distilled_training_data.csv'

pattern = 'NP: {<DT>?<JJ>*<NN>}'
# NP: none phrase
# DT: optional determiner
# JJ: number of adjectives
# NN: none

# ex = d4['5_search'][0]
# ex1 = remove_nonASCII(ex)
# ex2 = remove_specialChar(ex1)
# ex3 = remove_longNum(ex2)
# ex4 = loc_tagging(ex3)
# ex5 = remove_SPACY_NER(spacy_NE_BILUO_tagging(ex4), ex4)
# ex6 = remove_stopwords(ex5)

'''
def nltk_preprocess(sentence: str) -> List[tuple]:
    """This function applies word tokenization and part-of-speech tagging to a sentence.
    
    Args:
        sentence: from scrapped search results.
        
    Returns:
        sent: tokenized sentence.
    """
    sent = word_tokenize(sentence)
    sent = pos_tag(sent)
    return sent

def chunk_parser(pattern: str, sent: List[Tuple[str]]) -> nltk.Tree:
    """
    
    Args:
        pattern:
        sent:
        
    Returns:
        tree:
    """
    chunk_parser = nltk.RegexpParser(pattern)
    chunk_sent = chunk_parser.parse(sent)
    return chunk_sent

def iob_tagging(chunk_sent: nltk.Tree) -> List[Tuple[str]]:
    """
    
    """
    iob_tagged = tree2conlltags(chunk_sent)
    return iob_tagged

def add_category_label(sentence: str) -> nltk.Tree:
    """
    
    """
    ne_tree = nltk.ne_chunk(pos_tag(word_tokenize(sentence)))
    return ne_tree
'''
